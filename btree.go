package main

import (
	"fmt"

	"./btree"
)

func binaryTree() {
	fmt.Println("Answers for Assignment 1, \"Binary Tree Assignment\":")

	// Binary tree:
	//
	//		 10
	//	      /      \
	//	     20      30
	//          /  \    /
	//	   40  60  50
	tree := &btree.BNode{
		Val: 10,
		Left: &btree.BNode{
			Val: 20,
			Left: &btree.BNode{
				Val: 40,
			},
			Right: &btree.BNode{
				Val: 60,
			},
		},
		Right: &btree.BNode{
			Val: 30,
			Left: &btree.BNode{
				Val: 50,
			},
		},
	}
	fmt.Println("  In-order traversal: ", tree.InOrder())
	fmt.Println("  Pre-order traversal: ", tree.PreOrder())
	fmt.Println("  Post-order traversal: ", tree.PostOrder())
}
