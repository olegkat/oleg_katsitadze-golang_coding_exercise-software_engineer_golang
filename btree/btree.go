// Package btree implements binary trees and a few simple operations
// on them.
package btree

import (
	"errors"
	"fmt"
)

// Binary tree node.  A binary tree is represented by a BNode which is
// the root of the tree.
type BNode struct {
	Val   int
	Left  *BNode
	Right *BNode
}

// NewOrdered takes a slice of values and returns an ordered binary
// tree built with those values.  Empty slice will yield a nil tree.
func NewOrdered(vals []int) *BNode {
	if len(vals) == 0 {
		return nil
	}

	root := &BNode{Val: vals[0]}
	for _, val := range vals[1:] {
		root.InsertOrdered(val)
	}

	return root
}

// CmpTrees takes two root nodes and returns true iff the two nodes
// represent the same tree.
func CmpTrees(n1 *BNode, n2 *BNode) bool {
	if n1 == nil && n2 == nil {
		return true
	}

	if n1 == nil || n2 == nil {
		return false
	}

	if n1.Val != n2.Val {
		return false
	}

	if !CmpTrees(n1.Left, n2.Left) {
		return false
	}

	if !CmpTrees(n1.Right, n2.Right) {
		return false
	}

	return true
}

// InsertOrdered adds a node to an ordered binary tree preserving the
// tree order.
func (root *BNode) InsertOrdered(val int) error {
	if root == nil {
		return errors.New("Can't work on nil trees")
	}

	node := BNode{Val: val}
	cur := root
	for {
		switch {
		case cur.Val > val:
			if cur.Left == nil {
				cur.Left = &node
				return nil
			}
			cur = cur.Left

		case cur.Val < val:
			if cur.Right == nil {
				cur.Right = &node
				return nil
			}
			cur = cur.Right

		default: // cur.Val == val
			return errors.New(fmt.Sprintf("Can't insert %d, already in the tree", val))
		}
	}

	panic("Should never happen")
}

// Insert is an alias for InsertOrdered (as per assignment).
func (root *BNode) Insert(val int) error {
	return root.InsertOrdered(val)
}

// InOrder returns a list of values containing the in-order traversal
// of the tree.
func (root *BNode) InOrder() []int {
	list := make([]int, 0)

	if root != nil {
		list = append(list, root.Left.InOrder()...)
		list = append(list, root.Val)
		list = append(list, root.Right.InOrder()...)
	}

	return list
}

// PreOrder returns a list of values containing the pre-order
// traversal of the tree.
func (root *BNode) PreOrder() []int {
	list := make([]int, 0)

	if root != nil {
		list = append(list, root.Val)
		list = append(list, root.Left.PreOrder()...)
		list = append(list, root.Right.PreOrder()...)
	}

	return list
}

// PostOrder returns a list of values containing the post-order
// traversal of the tree.
func (root *BNode) PostOrder() []int {
	list := make([]int, 0)

	if root != nil {
		list = append(list, root.Left.PostOrder()...)
		list = append(list, root.Right.PostOrder()...)
		list = append(list, root.Val)
	}

	return list
}
