package btree

import "testing"

func TestNewOrdered(t *testing.T) {
	if !CmpTrees(
		NewOrdered([]int{}),
		nil,
	) {
		t.Error()
	}

	if !CmpTrees(
		NewOrdered([]int{2}),
		&BNode{Val: 2},
	) {
		t.Error()
	}

	//             4
	//	     /   \
	//          2     5
	//         /  \    \
	//        1    3    7
	//                 /
	//                6
	if !CmpTrees(
		NewOrdered([]int{4, 2, 3, 5, 1, 7, 6}),
		&BNode{
			Val: 4,
			Left: &BNode{
				Val:   2,
				Left:  &BNode{Val: 1},
				Right: &BNode{Val: 3},
			},
			Right: &BNode{
				Val: 5,
				Right: &BNode{
					Val:  7,
					Left: &BNode{Val: 6},
				},
			},
		}) {
		t.Error()
	}
}

func TestCmpTrees(t *testing.T) {
	if !CmpTrees(nil, nil) {
		t.Error()
	}

	if CmpTrees(nil, &BNode{}) {
		t.Error()
	}

	if !CmpTrees(&BNode{}, &BNode{}) {
		t.Error()
	}

	if CmpTrees(&BNode{}, &BNode{Val: 1}) {
		t.Error()
	}

	if CmpTrees(&BNode{}, &BNode{Left: &BNode{}}) {
		t.Error()
	}

	if CmpTrees(&BNode{Right: &BNode{}}, &BNode{Left: &BNode{}}) {
		t.Error()
	}

	if CmpTrees(&BNode{Left: &BNode{Val: 1}}, &BNode{Left: &BNode{}}) {
		t.Error()
	}
}

func TestInsertOrdered(t *testing.T) {
	var tree *BNode

	err := tree.InsertOrdered(1)
	if err == nil {
		t.Error()
	}

	tree = &BNode{Val: 3}

	if tree.InsertOrdered(5) != nil {
		t.Error()
	}
	if !CmpTrees(tree, &BNode{
		Val:   3,
		Right: &BNode{Val: 5},
	}) {
		t.Error()
	}

	if tree.Insert(1) != nil {
		t.Error()
	}
	if !CmpTrees(tree, &BNode{
		Val:   3,
		Left:  &BNode{Val: 1},
		Right: &BNode{Val: 5},
	}) {
		t.Error()
	}

	if tree.InsertOrdered(4) != nil {
		t.Error()
	}
	if !CmpTrees(tree, &BNode{
		Val:  3,
		Left: &BNode{Val: 1},
		Right: &BNode{
			Val:  5,
			Left: &BNode{Val: 4},
		},
	}) {
		t.Error()
	}

	if tree.InsertOrdered(5) == nil {
		t.Error()
	}
}

func cmpSlices(s1 []int, s2 []int) bool {
	if len(s1) != len(s2) {
		return false
	}

	for i, v := range s1 {
		if v != s2[i] {
			return false
		}
	}

	return true
}

func TestTraversals(t *testing.T) {
	var nilTree *BNode

	if !cmpSlices(nilTree.InOrder(), []int{}) {
		t.Error()
	}

	if !cmpSlices(nilTree.PreOrder(), []int{}) {
		t.Error()
	}

	if !cmpSlices(nilTree.PostOrder(), []int{}) {
		t.Error()
	}

	//		 50
	//	      /      \
	//	     30      70
	//          /  \    /
	//	   10  40  60
	tree := NewOrdered([]int{50, 30, 10, 70, 40, 60})

	if !cmpSlices(tree.InOrder(), []int{10, 30, 40, 50, 60, 70}) {
		t.Error()
	}

	if !cmpSlices(tree.PreOrder(), []int{50, 30, 10, 40, 70, 60}) {
		t.Error()
	}

	if !cmpSlices(tree.PostOrder(), []int{10, 40, 30, 60, 70, 50}) {
		t.Error()
	}
}
