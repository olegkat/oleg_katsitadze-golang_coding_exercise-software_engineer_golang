package main

import (
	"fmt"

	. "./hrobber"
)

func houseRobber() {
	fmt.Println("Answers for Assignment 2, \"House Robber\":")
	fmt.Println("  ", RobCircular([]uint{2, 3, 2}))
	fmt.Println("  ", RobCircular([]uint{1, 2, 3, 1}))
	fmt.Println("  ", RobCircular([]uint{0}))
}
