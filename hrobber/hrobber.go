// Package hrobber provides functions to solve the house robbing
// problem.
//
// The setup:  You are a professional robber planning to rob houses
// along a street.  Each house has a certain amount of money stashed.
// All houses at this place are arranged in a circle.  That means the
// first house is the neighbor of the last one.  Meanwhile, adjacent
// houses have a security system connected, and it will automatically
// contact the police if two adjacent houses were broken into on the
// same night.
package hrobber

// RobCircular takes a slice of the amounts of money stashed in the
// houses and returns the maximum amount that can be robbed, assuming
// circular arrangement of the houses.
func RobCircular(stashed []uint) uint {
	// Get the trivial cases out of the way.
	if len(stashed) < 4 {
		return maxVal(stashed...)
	}

	// At most two houses will ever be skipped; if three or more
	// houses were to be skipped, at least one of those houses
	// will not be adjacent to the houses getting robbed, and
	// therefore can be robbed thus improving (or at least not
	// impairing) the total robbed amount.
	//
	// This means that at least one of the houses #1, #2 or #3
	// will always be robbed.
	//
	// Therefore, we can solve the problem by choosing the optimal
	// of the three cases:
	//
	// case 1:  rob #1, then optimally rob [#3, ..., #(N-1)];
	// case 2:  rob #2, then optimally rob [#4, ..., #N];
	// case 3:  rob #3, then optimally rob [#5, ..., #N];
	//
	// recursively solving each of the sub-problems without regard
	// for the circular housing arrangement.  We don't include
	// house #1 in the sub-problem for case 3, because a solution
	// with house #1 getting robbed will be part of case 1 with
	// house #3 getting robbed.
	return maxVal(
		stashed[0]+RobLinear(stashed[2:len(stashed)-1]),
		stashed[1]+RobLinear(stashed[3:]),
		stashed[2]+RobLinear(stashed[4:]),
	)
}

// RobLinear takes a slice of the amounts of money stashed in the
// houses and returns the maximum amount that can be robbed, assuming
// linear arrangement of the houses.
func RobLinear(stashed []uint) uint {
	switch len(stashed) {
	case 0:
		return 0

	case 1:
		return stashed[0]

	default:
		return maxVal(
			stashed[0]+RobLinear(stashed[2:]),
			RobLinear(stashed[1:]),
		)
	}
}

func maxVal(vals ...uint) (max uint) {
	for _, val := range vals {
		if val > max {
			max = val
		}
	}

	return
}
