package hrobber

import "testing"

type robTest struct {
	sum     uint
	stashes []uint
}

var RobCircularTests = []robTest{
	{0, []uint{}},
	{2, []uint{2}},
	{2, []uint{1, 2}},
	{3, []uint{3, 1, 2}},
	{4, []uint{2, 2, 1, 2}},
	{4, []uint{1, 0, 2, 4}},
	{5, []uint{2, 2, 1, 2, 3}},
	{8, []uint{3, 2, 3, 1, 2, 3}},
	{6, []uint{0, 2, 3, 2, 0, 2}},
	{9, []uint{1, 1, 4, 1, 1, 5}},
	{4, []uint{1, 0, 0, 0, 3, 0}},
	{18, []uint{2, 1, 1, 4, 3, 4, 7, 9}},
	{17, []uint{2, 1, 1, 3, 4, 4, 7, 9}},
	{16, []uint{2, 1, 1, 3, 4, 4, 9, 7}},
}

func TestRobCircular(t *testing.T) {
	for _, test := range RobCircularTests {
		got := RobCircular(test.stashes)
		if got != test.sum {
			t.Errorf("%v:  want %d, got %d",
				test.stashes,
				test.sum,
				got)
		}
	}
}

var RobLinearTests = []robTest{
	{0, []uint{}},
	{3, []uint{3}},
	{3, []uint{1, 3}},
	{3, []uint{2, 3, 1}},
	{4, []uint{2, 1, 0, 2}},
	{5, []uint{2, 0, 1, 1, 2}},
	{6, []uint{2, 0, 1, 3, 2, 1}},
	{9, []uint{3, 2, 3, 1, 2, 3}},
}

func TestRobLinear(t *testing.T) {
	for _, test := range RobLinearTests {
		got := RobLinear(test.stashes)
		if got != test.sum {
			t.Errorf("%v:  want %d, got %d",
				test.stashes,
				test.sum,
				got)
		}
	}
}
